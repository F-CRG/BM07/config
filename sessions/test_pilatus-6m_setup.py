
from bliss.setup_globals import *

load_script("test_pilatus-6m.py")

print("")
print("Welcome to your new 'test_pilatus-6m' BLISS session !! ")
print("")
print("You can now customize your 'test_pilatus-6m' session by changing files:")
print("   * /test_pilatus-6m_setup.py ")
print("   * /test_pilatus-6m.yml ")
print("   * /scripts/test_pilatus-6m.py ")
print("")