from bliss.setup_globals import *
load_script("fipsetup.py")

print("")
print("Welcome to your new 'fipsetup' BLISS session !! ")
print("")
print("You can now customize your 'fipsetup' session by changing files:")
print("   * /fipsetup_setup.py ")
print("   * /fipsetup.yml ")
print("   * /scripts/fipsetup.py ")
print("")

