
from bliss.setup_globals import *
#from bm07 import grob_FIP


load_script("fipcontrol.py")
from bm07.wa_fip import wa as wafip
from bm07.reset_motor import *
from bm07.test import *
from bm07.wago_test_pneumatique import *
from bm07.find_max_att import find_max_attenuation
from bm07.safety import *
from bm07.flash_gordon import *
from bm07.hutch_actions import *

fipcontrol.disable_esrf_data_policy()

print("")
print("Welcome to your new 'fipcontrol' BLISS session !! ")
print("")
print("You can now customize your 'fipcontrol' session by changing files:")
print("   * /fipcontrol_setup.py ")
print("   * /fipcontrol.yml ")
print("   * /scripts/fipcontrol.py ")
print("")

#uncomment to add a homing of the fastshutter at each bliss start:
# if detcover.isClose():
#     fastshutmot.homing()