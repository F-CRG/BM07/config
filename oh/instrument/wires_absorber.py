import scipy.integrate as integrate


def probaE():
    #KeV Flux
    tabFlux = []
    tabFlux.append([0,   3,00E+13])
    tabFlux.append([0.304321363359708,   7.00E+13)
    tabFlux.append([0.608642726719416,   1.00E+14)
    tabFlux.append([2.06938527084601,    2.00E+14)
    tabFlux.append([4.44309190505174,    3.00E+14)
    tabFlux.append([7.30371272063299,    3.60E+14)
    tabFlux.append([10.7729762629337,    4.00E+14)
    tabFlux.append([13.3901399878271,    4.10E+14)
    tabFlux.append([16.8594035301278,    4.00E+14)
    tabFlux.append([30.9799147900183,    3.00E+14)
    tabFlux.append([44.3091905051735,    2.00E+14)
    tabFlux.append([96.2264150943396,    3.00E+13)

    in